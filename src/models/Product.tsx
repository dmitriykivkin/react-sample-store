export default interface IProduct {
    id: string|number,
    title: string,
    imageUrl: string,
    price: number
}