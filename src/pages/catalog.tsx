import React, { useEffect, useState} from 'react'
import { useStore } from 'react-redux'
import ProductListComponent from '../components/catalog/products-list'
import FiltersListComponent from '../components/catalog/filters-list'
import styled from 'styled-components'
import { getProductsAction } from '../store/actions/catalog-product-actions'
import { useDispatch } from 'react-redux'
import { getProductsSelector } from '../store/selectors/catalog-selectors'
import IProduct from '../models/Product'

function CatalogPageComponent(props:any) {

    const dispatch = useDispatch()
    dispatch(getProductsAction({}))

    //const [products, setProducts] = useState<IProduct[]>([])
    const { getState } = useStore()
    const state = getState()
    const products: IProduct[] = getProductsSelector(state)
    //setProducts(getProductsSelector(state))

    /*useEffect(()=>{
        
    })*/

    //styles
    const Page = styled.div`
    
    `
    const Container = styled.div`
        margin: 0 auto;
        max-width: 1400px;
    `

    const MainContent = styled.div`
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    `

    return (
        <Page>
            <h1>CATALOG PAGE HERE!</h1>
            <Container>
                <MainContent>
                    <FiltersListComponent />
                    <ProductListComponent items={products}/>
                </MainContent>
            </Container>
        </Page>
    )
}

export default CatalogPageComponent