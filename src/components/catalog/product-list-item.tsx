import React from 'react'
import NumberFormat from 'react-number-format'
import NumberFromat from 'react-number-format'
import styled from 'styled-components'

interface IProductListItemProps {
    product: {id: number|string, title: string, imageUrl?: string, price?: number}
}

const ProductListItemComponent = (props: IProductListItemProps) => {

    const ItemSection = styled.div`
        padding: 12px;
        max-width: 160px;        
    `

    const ImageBlock = styled.div`
        & > img {
            max-width: 160px;
        }
    `

    const TitleBlock = styled.div`
      text-align: center;
    `
    const PriceBlock = styled.div`
      text-align: center;
    `

    const ActionsBlock = styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
    `

    const AddToCartButton = styled.button`
        background:blue;
        color: white;
    `


    return (
        <ItemSection>
            <ImageBlock>
                <img src={props.product.imageUrl}></img>
            </ImageBlock>
            <TitleBlock>
                <span>{props.product.title}</span>
            </TitleBlock>
            <PriceBlock>
                {/*Todo replace this shitty lib */}
                <span><NumberFormat 
                    value={props.product.price} 
                    displayType={'text'} 
                    prefix={'$'} 
                    fixedDecimalScale = {true}
                    thousandSeparator="," 
                    isNumericString={true}
                    renderText={value => <div>{value}</div>}
                /></span>
            </PriceBlock>
            <ActionsBlock>
                <AddToCartButton>
                    Add To Cart
                </AddToCartButton>
            </ActionsBlock>
        </ItemSection>
    )
}

export default ProductListItemComponent

