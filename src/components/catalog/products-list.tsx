import React from 'react'
import ProductListItemComponent from './product-list-item'
import styled from 'styled-components' 
import IProduct from '../../models/Product'

interface IProductListProps {
    items: IProduct[]
}

const ProductListComponent = (props: IProductListProps) => {

    //styles

    const List = styled.div`
        display:flex;
        flex-direction: row;
        justify-content: space-between;
        flex-flow: wrap;
        max-width: 1000px
        & after {
            content: ""
            flex: auto
        }
        
    `

    const ListBottomPlaceHolder = styled.div`
        flex-grow: 1
    `

    return (
        <List>
            {console.log(props)}
            {props.items.map((item, index) => {
                return <ProductListItemComponent product={item} key={index}/>
            })
            }
        </List>
    )
}

export default ProductListComponent 