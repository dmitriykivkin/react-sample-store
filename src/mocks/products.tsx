import IProduct from '../models/Product'

export const productsCollection: IProduct[] = [
    {
        id: 1,
        title: 'Awesome Product 1',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 50000
    },
    {
        id: 2,
        title: 'Awesome Product 2',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 46000
    },
    {
        id: 3,
        title: 'Awesome Product 3',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 42000
    },
    {
        id: 4,
        title: 'Awesome Product 4',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 38000
    },
    {
        id: 5,
        title: 'Awesome Product 5',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 34000
    },
    {
        id: 6,
        title: 'Awesome Product 6',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 30000
    },
    {
        id: 7,
        title: 'Awesome Product 7',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 26000
    },
    {
        id: 8,
        title: 'Awesome Product 8',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 22000
    },
    {
        id: 9,
        title: 'Awesome Product 9',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 18000
    },
    {
        id: 10,
        title: 'Awesome Product 10',
        imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png',
        price: 14000
    },

]