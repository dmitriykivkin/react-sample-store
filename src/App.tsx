import React from 'react';
import CatalogPageComponent from '../src/pages/catalog';
import HomeDefaultPage from '../src/pages/home-default';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import './App.css';

function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/catalog">Catalog</Link>
            </li>
          </ul>
        </nav>
        </div>
      <Switch>
        <Route path="/catalog">
          <CatalogPageComponent />
        </Route>
        <Route path="/">
          <HomeDefaultPage />
        </Route>
      </Switch>
    </Router>
    
  );
}

export default App;
