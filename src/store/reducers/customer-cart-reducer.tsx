import {
    ECustomerCartActions,
    ICartItem
} from '../actions/customer-cart-actions'
import IAction from '../actions/action'

export interface ICustomerCartState {
    items: ICartItem [],
    isFetching: boolean
}

const initialState: ICustomerCartState = {
    items: [],
    isFetching: false
}

export function customerCartReducer(state: ICustomerCartState = initialState, action:IAction) {

    switch (action.type) {
        case ECustomerCartActions.ADD_ITEM: {
            return { ...state, isFetching: true }
            break
        }
        case ECustomerCartActions.ADD_ITEM_SUCCESS: {
            state.items.push(action.payload)
            break
        }
        case ECustomerCartActions.ADD_ITEM_FAIL: {
            break;
        }

        case ECustomerCartActions.REMOVE_ITEM: {
            break
        }
        case ECustomerCartActions.UPDATE_ITEM_FAIL: {
            break
        }
        default:
            return state
    }

}