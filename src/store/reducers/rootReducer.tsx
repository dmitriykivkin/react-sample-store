import { combineReducers } from 'redux'
import { catalogProductsReducer } from './catalog-products-reducer'

const rootReducer = combineReducers({
    catalogState: catalogProductsReducer,
})

export default rootReducer