import IProduct from '../../models/Product'
import IAction from '../actions/action'
import {
    ECatalogProductActions
} from '../actions/catalog-product-actions'

export interface ICatalogState {
    currentProducts: IProduct[],
    isFetching: boolean
}

const initialState: ICatalogState = {
    currentProducts: [],
    isFetching: false
}

export function catalogProductsReducer(state = initialState, action: IAction) {
    switch(action.type) {
        case ECatalogProductActions.GET_PRODUCTS: {
            console.log('reducer works')
            return {...state, isFetching: true}
            break
        }
        case ECatalogProductActions.GET_PRODUCTS_SUCCESS: {
            console.log('reducer works like a charm')
            return {...state, currentProducts: action.payload, isFetching: false}
            break
        }
        case ECatalogProductActions.GET_PRODUCTS_FAIL: {
            break
        }
        default:
            return state;
    }
}