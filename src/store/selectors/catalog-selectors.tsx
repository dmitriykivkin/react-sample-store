import {IState, ICatalogState} from '../state/state'
import { createSelector } from 'reselect'
import IProduct from '../../models/Product'

export const getProductsSelector = (state:IState): IProduct[] => {
    return state.catalogState.currentProducts
}