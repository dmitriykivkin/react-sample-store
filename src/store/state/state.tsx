import IProduct from '../../models/Product'

export interface IState {
    catalogState: ICatalogState
}

export interface ICatalogState {
    currentProducts:IProduct[]
    isFetching: boolean
}