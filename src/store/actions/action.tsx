export default interface IAction {
    type: string|number,
    payload?: any
}