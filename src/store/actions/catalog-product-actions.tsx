import IAction from './action'
import IProduct from '../../models/Product'

export enum ECatalogProductActions {
    GET_PRODUCTS = 'GET_PRODUCTS',
    GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS',
    GET_PRODUCTS_FAIL = 'GET_PRODUCTS_FAIL',
}

export function getProductsAction(payload: any):IAction {
    return {
        type: ECatalogProductActions.GET_PRODUCTS,
        payload
    }
}

export function getProductsSuccessAction(payload: IProduct[]):IAction {
    return {
        type: ECatalogProductActions.GET_PRODUCTS_SUCCESS,
        payload
    }
}

export function getProductsFailAction(payload: any):IAction {
    return {
        type: ECatalogProductActions.GET_PRODUCTS_FAIL,
        payload
    }
}