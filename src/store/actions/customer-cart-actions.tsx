import IAction from './action'

export enum ECustomerCartActions {
    ADD_ITEM = 'ADD_ITEM',
    ADD_ITEM_SUCCESS = 'ADD_ITEM_SUCCESS',
    ADD_ITEM_FAIL = 'ADD_ITEM_FAIL',

    REMOVE_ITEM = 'REMOVE_ITEM',
    REMOVE_ITEM_SUCCESS = 'REMOVE_ITEM_SUCCESS',
    REMOVE_ITEM_FAIL = 'REMOVE_ITEM_FAIL',

    UPDATE_ITEM = 'UPDATE_ITEM',
    UPDATE_ITEM_SUCCESS = 'UPDATE_ITEM_SUCCESS',
    UPDATE_ITEM_FAIL = 'UPDATE_ITEM_FAIL'
}
//Todo move this interface to the more appropriate place
export interface ICartItem {
    id: string|number,
    productId: string|number,
    quantity: number
}


export interface IAddItemPayload {
    productId: string|number,
    quantity: number
}

export interface IRemoveItemPayload {
    itemId: string|number
}

export interface IUpdateItemPayload {
    itemId: string|number,
    quantity: number
}

export function AddItemAction(payload: IAddItemPayload):IAction {
    return {
        type: ECustomerCartActions.ADD_ITEM,
        payload
    }
}

export function AddItemActionSuccess(payload: ICartItem): IAction {
    return {
        type: ECustomerCartActions.ADD_ITEM_SUCCESS,
        payload
    }
}

export function AddItemActionFail(payload: any): IAction {
    return {
        type: ECustomerCartActions.ADD_ITEM_FAIL,
        payload
    }
}

export function RemoveItemAction(payload: IRemoveItemPayload): IAction {
    return {
        type: ECustomerCartActions.REMOVE_ITEM,
        payload
    }
}

export function RemoveItemActionSuccess(): IAction {
    return {
        type: ECustomerCartActions.REMOVE_ITEM_SUCCESS
    }
}

export function RemoveItemActionFail(payload: any): IAction {
    return {
        type: ECustomerCartActions.REMOVE_ITEM_FAIL,
        payload
    }
}

export function UpdateItemAction(payload: IUpdateItemPayload): IAction {
    return {
        type: ECustomerCartActions.UPDATE_ITEM,
        payload
    }
}

export function UpdateItemActionSuccess(payload: ICartItem): IAction {
    return {
        type: ECustomerCartActions.UPDATE_ITEM_SUCCESS,
        payload
    }
}

export function UpdateItemActionFail(payload: any): IAction {
    return {
        type: ECustomerCartActions.UPDATE_ITEM_FAIL,
        payload
    }
}