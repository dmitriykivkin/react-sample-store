import { all } from 'redux-saga/effects'
import { watchGetProducts } from './catalog-products-sagas'

export default function* rootSaga() {
    yield all([
        watchGetProducts()
    ])
}