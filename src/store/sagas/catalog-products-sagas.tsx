import {call, put, takeLatest, takeEvery} from 'redux-saga/effects'
import {productsCollection} from '../../mocks/products'

import {
    ECatalogProductActions,
    getProductsSuccessAction,
    getProductsFailAction
} from '../actions/catalog-product-actions'

function* getProducts() {
    try {
        //fetch data here
        console.log('saga is working')
        let data = productsCollection
        yield put(getProductsSuccessAction(data))
    }catch(ex: any) {
        if(ex instanceof Error){
            yield put(getProductsFailAction(ex.message))
        } else {
            yield put(getProductsFailAction('unknown error happened'))
        }
    }
}

export function* watchGetProducts() {
    yield takeEvery(ECatalogProductActions.GET_PRODUCTS, getProducts)
}

