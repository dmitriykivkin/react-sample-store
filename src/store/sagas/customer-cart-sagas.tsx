import {call, put, takeEvery} from 'redux-saga/effects';

import {
    ECustomerCartActions,
    AddItemActionSuccess,
    AddItemActionFail,
    AddItemAction
} from '../actions/customer-cart-actions' 

export function* watchAddItem() {
    yield takeEvery(ECustomerCartActions.ADD_ITEM, addItem)
}

function* addItem() {
    //fetch data here
    yield put (AddItemActionSuccess({id:1, productId: 10, quantity:2 }))
}

